<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_Model extends CI_Model {
    //Load category
    function loadCategory(){
        $query = $this->db->query("SELECT * FROM category");
        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }
    //Add category to the database
    function saveCategory($name){
        $query=$this->db->query("INSERT INTO `category`(`id_category`, `name_category`) VALUES ('','$name')");
        if($query){
            return true;
        }
    }
    //Edit database category
    function editCategory($id,$name){
        $query=$this->db->query("UPDATE category SET `name_category` = '$name' WHERE id_category = '$id'");
        if($query){
            return true;
        }
    }
    //Delete category of the database
    function deleteCategory($id){
        $query=$this->db->query("DELETE FROM category WHERE id_Category = '$id'");
        if($query){
            return true;
        }else{
            return false;
        }
    }
    public function distintCategory($id){
        //Query that filters the new sources according to the logged in user
        $query = $this->db->query("SELECT DISTINCT news.category_id, category.name_category FROM news 
        INNER JOIN category ON news.category_id = category.id_category WHERE news.user_id = ".$id."");
        if($query){
            return $query->result_array();
        }
    }
}