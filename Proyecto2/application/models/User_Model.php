<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {
    //Function to save user
    public function saveUser($user){
        $this->db->set('email', $user['email']);
        $this->db->set('first_name', $user['first_Name']);
        $this->db->set('last_name', $user['last_Name']);
        $this->db->set('passw', $user['password']);
        $this->db->set('perfil', 0);
        $this->db->set('role_id', '2');
        $query = $this->db->insert('users');
        if ($this->db->affected_rows() > 0) {
          return true;
        }else {
          return false;
        }
    }
    //Function to authenticate user
    public function authenticate($email,$password){
        $query = $this->db->query("SELECT users.id_User,users.email, users.passw, users.first_name, users.last_name,users.perfil,users.portal, roles.name FROM users 
    INNER JOIN roles ON users.role_id = roles.id_role WHERE
     email = '$email' AND passw = '$password'");
        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }
    //Function to activate the user's account
    public function activatePerfil($email){
        $query = $this->db->query("UPDATE users SET perfil = 1 WHERE email = '$email'");
        if($query){
            $query = $this->db->query("SELECT * FROM users WHERE email = '$email'");
            if($query){
                return $query->result_array();
            }
        }
    }
    //Change the status portal (public or private)
    public function changeStatePortal($id,$state){
        $query = $this->db->query("UPDATE users SET `portal` = '$state' WHERE id_User = $id");
        if($query){
            return true;
        }
    }
}