<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_Model extends CI_Model {
///Save the news obtained through rss in the database
public function feedBD($sql){
    $query = $this->db->query($sql);
     if ($query) {
        return true;
    } else {
        return false;
    }
}
//Funtion to charge the news by user or by user and category
public function newsLoad($id_user, $id_category){
    if($id_category==='0'){
        //Query that filters the new sources according to the logged in user
        $query = $this->db->query("SELECT news.id_news, news.title, news.short_description, news.permanlink, 
                news.imagen_url,news.sdate, category.name_category FROM news, category 
                WHERE news.user_id = $id_user AND news.category_id = category.id_category 
                ORDER BY news.sdate DESC");
        if($query){
            return $query->result_array();
        }
    }else{
        $query = $this->db->query("SELECT news.id_news, news.title, news.short_description, news.permanlink, 
                news.imagen_url,news.sdate, category.name_category
                FROM news INNER JOIN category ON news.category_id = category.id_category  
                WHERE news.user_id = $id_user AND news.category_id = $id_category
                ORDER BY news.sdate DESC");
        if($query){
            return $query->result_array();
        }
    }
}
    //Funtion to search news in the database with words specific in the news 
    public function searchNews($id_User,$term){
        $query = $this->db->query("SELECT news.id_news, news.title, news.short_description, news.permanlink, 
        news.imagen_url,news.sdate, category.name_category
        FROM news INNER JOIN category ON news.category_id = category.id_category  
        WHERE news.user_id = $id_User AND short_description LIKE '%$term%' ORDER BY news.sdate");
        if($query){
            return $query->result_array();
        }
    }
    ///See if the category you want to delete is in the news
    public function categoryINnews($id){
        $query = $this->db->query("SELECT * FROM news WHERE category_id = $id");
        if($query){
            return false;
        }else{
            return true;
        }
    }
    //Confirm if portal is public or not
    public function confirmationStatePortal($first_Name, $last_name){
        $query = $this->db->query("SELECT users.portal, users.id_User FROM users WHERE first_name = '$first_Name' AND last_name = '$last_name'");
        if($query){
            return $query->result_array();
        }
    }
}