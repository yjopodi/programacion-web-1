<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NSource_Model extends CI_Model {
    //read sources from the databaseLoad sources
    public function loadNewSource($id){
        $query = $this->db->query("SELECT newsource.id_newsource,newsource.url,newsource.user_id,newsource.name, category.name_category FROM newsource 
        INNER JOIN category ON newsource.category_id = category.id_category WHERE newsource.user_id = '$id'");
        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }
    //Add sources to the database
    public function saveSource($url, $name, $category, $user){
        
        $query=$this->db->query("INSERT INTO `newsource`(`id_newsource`, `url`, `name`, `category_id`, `user_id`)
         VALUES ('','$url','$name','$category','$user')");
        if($query){
            return true;
        }
    }
    //Edit sources to the database
    public function editSource($idUpdat, $nameUpdat,$urlR,$categ){
        $query=$this->db->query("UPDATE newsource SET `name` = '$nameUpdat',`url` = '$urlR',category_id = $categ WHERE id_newsource = '$idUpdat'");
        if($query){
            return true;
        }
    }
    //Delete sources to the database
    public function deleteNewSource($id){
        $query=$this->db->query("DELETE FROM newsource WHERE id_newsource = $id");
        if($query){
            return true;
        }
    }
    
}
