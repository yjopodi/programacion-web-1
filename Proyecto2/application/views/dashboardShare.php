<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Friend News</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="<?php echo base_url();?>css/estilos.css">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
</head>
<body>
<div class = "container">
    <div class = "row">
        <div class= "col-md-12">
            <div class = "moverImgMycLogin">
                <img src="<?php echo base_url();?>img/ncover.png" alt="">
            </div>
        </div>
    </div>
    <header class = "row">
        <div class = "col-md-12 text-center">
            <h2>Your Friends News Cover</h1>
        </div>
    </header>
    <div class = "row justify-content-center">
        <div class="col-md-3">
            <hr>
        </div>
    </div>
    <div class = "row">
        <?php
                //recorre las filas
                foreach($categories_news[1] as $new){
                    $img=$new['imagen_url'];
                    if($img=="NO"){
                        $img= base_url()."img/reg.png";   
                    }
                    echo '<div class = "col-md-4">
                            <div class = "row">
                                <div class = col-md-5>
                                    <p class = "nFecha">'.$new['sdate'].'</p>
                                </div>
                            </div>
                            <div class = "row cont">
                                <div class = "col-md-6 ex2">
                                    <a href="'.$new['permanlink'].'"><img class = "redimension" src="'.$img.'" alt=""></a>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = col-md-8>
                                    <a href="'.$new['permanlink'].'"><p class = "nTitulo"><strong>'.$new['title'].'</strong></p></a>
                                </div>
                                <div class = col-md-4>
                                <p class = "nCategoria">'.$new['name_category'].'</p>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = col-md-10>
                                    <p class =  "nDescripcion">'.$new['short_description'].'</p>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = col-md-6>
                                    <a href="'.$new['permanlink'].'">Ver Noticia</a>
                                </div>
                            </div>
                        </div>';  
            }?>
    
    </div>
</div>
<hr>
<nav>
    <div id = "barraBajaLogin"> 
        <ul>
            <li><a href="inicio.html" >My cover</a></li> 
            <li><a href="#" >|</a></li>
            <li><a href="cambalache.html" >About</a></li>
            <li><a href="#" >|</a></li>
            <li><a href="login.html" >Help</a></li>
        </ul>
    </div>
</nav>
<footer>
    <div id = "imgLoginC">
        <img src="<?php echo base_url();?>img/c.png" alt="My news cover">
        <h3>My news cover</h3>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
</body>
</html>