<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    public function login()
	{
		$this->load->view('login');
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url(['User','login']));
	}
	public function registro(){
		$this->load->view('registro');
	}
	public function dashboard(){
		$id_Category = '0';//starts from zero to load the portal with all the news
		$user = $this->session->user_session;
		if(!$user){
			redirect(site_url(['User','login']));
		}
        $id_User = $user[0]['id_User'];
        $loading_news = $this->News_Model->newsLoad($id_User,$id_Category);//The news is brought from the model to be sent to view
        if(empty($loading_news)){
            redirect(site_url(['NewSource','sources']));
        }else{
        	$categories = $this->Category_Model->distintCategory($id_User);//The category is brought from the model to be sent to view
            $data['categories_news']= array_merge(array($categories),array($loading_news));//The two arrays are combined to be sent to the view as one since 2 cannot be sent separately
            $this->load->view('dashboard',$data);
        }
	}
	//Function add user in the database and call send mail function
	public function addUser(){
		$this->load->library('form_validation');
		$register_user = $this->input->post('signup');
		if(isset($register_user)) {
			$this->form_validation->set_rules('first_Name', 'First Name', 'required');
			$this->form_validation->set_rules('last_Name', 'Last Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if ($this->form_validation->run() == TRUE) {
				$user = $this->input->post();
				$user_registrated = $this->User_Model->saveUser($user);
				if($user_registrated) {
					$EMAIL = User::send_mail($user);
					if($EMAIL){
					redirect(site_url(['User','login']));
					}
				}
			}else{
			redirect(site_url(['User','registro']));
			}
		}
	}
	//Function to validate that the user is in the database with the email and password, 
	//redirects the user to their respective page according to their type of
	public function authenticate_login(){
		$this->load->library('form_validation');
		$authenticate_user= $this->input->post('login');
		if(isset($authenticate_user)){
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('pass', 'Password', 'required');
			if ($this->form_validation->run() == TRUE) {
				$user = $this->input->post();
				$user_registrated = $this->User_Model->authenticate($user['email'], $user['pass']);
				if($user_registrated){
					$this->session->set_userdata('user_session',$user_registrated);
					if($user_registrated[0]['name'] === 'Administrador'){
						redirect(site_url(['Category','categories']));
					} else{
						redirect(site_url(['User','dashboard']));
					}
					
				} else {
					redirect(site_url(['User','login']));
				}
			}
		}
	}
	//This function receives the email by parameter and activates the user's profile, the function automatically logs in
	public function validateEmail($email){
		$activar = $this->User_Model->activatePerfil($email);
		if($activar){
			$this->session->set_userdata('user_session',$activar);
				if($activar[0]['name'] === 'Administrador'){
					redirect(site_url(['Category','categories']));
				} else{
					redirect(site_url(['User','dashboard']));
				}
		}else {
			redirect(site_url(['User','login']));
		}
	}
	//Function to send mail to the user with the access link
	private function send_mail($user){
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'yjopodi@gmail.com',
			'smtp_pass' => 'yordy18$', 
			'smtp_port' => 465,
			'mailtype' => 'html',
			'wordwrap' => TRUE,
			'charset' => 'iso-8859-1'
			);

		$this->load->library('email', $config);
		$this->email->from('mycover@newscover.com', 'News Cover');
		$this->email->to($user['email']);
		$this->email->subject('News Cover');
		$message = '<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link rel="preconnect" href="https://fonts.gstatic.com">
			<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
			<title>MyNewsCover</title>
		</head>
		<body>
			<fieldset>
				<h1 style ="text-align: center; font-size: 36px; color: rgb(61, 60, 60); font-family: Trebuchet MS", Helvetica, sans-serif;">Hello ' .$user['first_Name'].'</h1>
				<p style = "text-align: center; font-family: Trebuchet MS", Helvetica, sans-serif; font-size: 18px; color: #3f4244;">
					We have received a request
					for the activation of your email address <br>
					To activate your account: 
				</p>
				<div style ="display: flex; justify-content: center;">
    				<img style = "max-width: 316px;" src="https://estaticos.elperiodico.com/resources/jpg/3/5/imagenes-blanco-negro-barcelona-confinada-1585504365753.jpg" alt="">
    			</div>
				<p style = "text-align: center; font-family: Trebuchet MS", Helvetica, sans-serif; font-size: 18px; color: #3f4244;">
					Please:  <strong><a style = "color: rgb(64, 100, 134);" href="'. site_url(['User','validateEmail',$user['email']]) .'">
        			Click here </a></strong>
				</p>
				<footer style"text-align: center;" >&copy; MyNewsCover.com</footer>
			</fieldset>
		</body> 
		</html>';
		$this->email->message($message);
		$this->email->set_newline("\r\n");
		$finish = $this->email->send();
		if($finish){
			return true;

		}
	}
	// Función para cambiar el estado del portal entre privado o público 
	public function changeState(){
		$this->load->library('form_validation');
		$register_user = $this->input->post('estado');
		if(isset($register_user)) {
			$this->form_validation->set_rules('change', 'Change', 'required');
			if ($this->form_validation->run() == TRUE) {
				$state = $this->input->post();
				$user = $this->session->user_session;
        		$id_User = $user[0]['id_User'];
				$portal_changeState = $this->User_Model->changeStatePortal($id_User,$state['change']);
				$newUser = $this->User_Model->authenticate($user[0]['email'], $user[0]['passw']);
				if($portal_changeState and $newUser) {
					$this->session->set_userdata('user_session',$newUser);
					redirect(site_url(['User','dashboard']));
				}
			}
		}
	}
	//Function to the share news with other users
	public function shareNews($first_Name, $last_name){
		$confirmation = $this->News_Model->confirmationStatePortal($first_Name,$last_name);
		if($confirmation[0]['portal'] === 'public'){
			$id_Category = '0';//starts from zero to load the portal with all the news
			$id_User = $confirmation[0]['id_User'];
			$loading_news = $this->News_Model->newsLoad($id_User,$id_Category);//The news is brought from the model to be sent to view
			$categories = $this->Category_Model->distintCategory($id_User);//The category is brought from the model to be sent to view
            $data['categories_news']= array_merge(array($categories),array($loading_news));//The two arrays are combined to be sent to the view as one since 2 cannot be sent separately
            $this->load->view('dashboardShare',$data);
		}else{
			$this->load->view('login');
		}
	}
}