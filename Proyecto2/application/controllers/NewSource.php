<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewSource extends CI_Controller {
    // Redirect to the view to add sources
    public function addSourceV()
	{
        $data['categories']= $this->Category_Model->loadCategory();
		$this->load->view('addNewSources',$data);
    }
    //Redirect to edit view to edit source name, url, and category
    public function editNewSourceV($id,$url,$userID,$name,$category){
        $categories = $this->Category_Model->loadCategory();
        $data['source'] = array("id" => $id, "url"=>$url, "user"=>$userID, "name"=>$name, "category"=>$category,$categories);
		$this->load->view('editNewSource',$data);
    }
    //Loads the sources that the user has added by means of the id in the view that contains the table with the same
    public function sources(){
        $user = $this->session->user_session;
        if(!$user){
			redirect(site_url(['User','login']));
		}
        $id = $user[0]['id_User'];
        $sources = $this->NSource_Model->loadNewSource($id);
        if(empty($sources)){
            $this->load->view('nsources');
        }else{
            $data['sources']=$sources;
            $this->load->view('nsources',$data);
        }

    }
    //Function to add sources that obtains the data from the view to send it 
    //to the database and add a new source
    public function addSource(){
        $this->load->library('form_validation');
		$register_category = $this->input->post('addSource');
		if(isset($register_category)) {
			$this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('url', 'URL', 'required');
            $this->form_validation->set_rules('category', 'Categoria', 'required');
            $this->form_validation->set_rules('idU', 'Usuario', 'required');
			if ($this->form_validation->run() == TRUE) {
				$source = $this->input->post();
                $url = $source['url'];
                $name = $source['name'];
                $category = $source['category'];
                $user = $source['idU'];
				$source_registrated = $this->NSource_Model->saveSource($url, $name, $category, $user);
				if($source_registrated) {
                    $clear_url = str_replace('/','$',$url);
					redirect(site_url(['News','addFeed',$clear_url, $category,$user]));
				}
            }
		}

    }
    //Function to add sources that obtains the data from
    // the view to send it to the database and edit a source
    public function editSource(){
        $this->load->library('form_validation');
        $edit_source = $this->input->post('update');
        if(isset($edit_source)) {
			$this->form_validation->set_rules('source_id', 'Id', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('url', 'URL', 'required');
            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('user_id', 'User', 'required');
            if ($this->form_validation->run() == TRUE) {
				$source_edit = $this->input->post();
				$source_edited = $this->NSource_Model->editSource($source_edit['source_id'],$source_edit['name'],
                $source_edit['url'],$source_edit['category']);
                if($source_edited) {
                    //replace one character with another so as not to
                    // cause errors when sending them through the url
					$clear_url = str_replace('/','$',$source_edit['url']);//
					redirect(site_url(['News','addFeed',$clear_url,$source_edit['category'], $source_edit['user_id']]));
				}
			}else{
			redirect(site_url(['NewSource','editNewSourceV']));
			}
        }
    }
    //Get the id to send it to the model 
    //to delete a new source from the database
    public function deleteSource($id){
        $source_deleted = $this->NSource_Model->deleteNewSource($id);
        if($source_deleted) {
            redirect(site_url(['NewSource','sources']));
        }
    }
    //Function to load the news in the dashboard according to the button pressed to select by category
    public function chargeNews(){
        $this->load->library('form_validation');
        $chargeNews = $this->input->post('categoriaID');
        if(isset($chargeNews)) {
			$this->form_validation->set_rules('categoriaID', 'Id', 'required');
            if ($this->form_validation->run() == TRUE) {
                $newsCharge = $this->input->post();
                $user = $this->session->user_session;
                $id_Category = $newsCharge['categoriaID'];
                $id_User = $user[0]['id_User'];
                $loading_news = $this->News_Model->newsLoad($id_User,$id_Category);
                if(empty($loading_news)){
                    redirect(site_url(['NewSource','sources']));
                }
                else{
                    $categories = $this->Category_Model->distintCategory($id_User);//The category is brought from the model to be sent to view
                    $data['categories_news']= array_merge(array($categories),array($loading_news));//The two arrays are combined to be sent to the view as one since 2 cannot be sent separately
                    $this->load->view('dashboard',$data);
                }
            }
        }
    }
    //Funtion to search news with word write in the search bar
    public function searchNews(){
        $this->load->library('form_validation');
        $searchNews = $this->input->post('search');
        if(isset($searchNews)) {
			$this->form_validation->set_rules('searchName', 'Search Name', 'required');
            if ($this->form_validation->run() == TRUE) {
                $newSearch = $this->input->post();
                $user = $this->session->user_session;
                $term = $newSearch['searchName'];
                $id_User = $user[0]['id_User'];
                $searching_news = $this->News_Model->searchNews($id_User,$term);
                if(empty($searching_news)){
                    $id_Category = '0';//starts from zero to load the portal with all the news
                    $loading_news = $this->News_Model->newsLoad($id_User,$id_Category);//The news is brought from the model to be sent to view
                    $categories = $this->Category_Model->distintCategory($id_User);//The category is brought from the model to be sent to view
                    $data['categories_news']= array_merge(array($categories),array($loading_news));//The two arrays are combined to be sent to the view as one since 2 cannot be sent separately
                    $this->load->view('dashboard',$data);
                }
                else{
                    $categories = $this->Category_Model->distintCategory($id_User);//The category is brought from the model to be sent to view
                    $data['categories_news']= array_merge(array($categories),array($searching_news));//The two arrays are combined to be sent to the view as one since 2 cannot be sent separately
                    $this->load->view('dashboard',$data);
                }
            }
        }
    }
}