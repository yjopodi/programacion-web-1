<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    ///Entry to the categories section by the administrator user
    public function categories(){
        $categories = $this->Category_Model->loadCategory();
        $data['categories']=$categories;
        $this->load->view('categories_dash',$data);
    }
    public function addCategoryV()
	{
		$this->load->view('addCategory');
	}
    public function editCategoryV($id,$name)
	{
        $data['category'] = array("id" => $id, "name"=>$name);
		$this->load->view('editCategory',$data);
	}
    //Getting data of form for add category in the database
    public function addCategory(){
        $this->load->library('form_validation');
		$register_category = $this->input->post('save');
		if(isset($register_category)) {
			$this->form_validation->set_rules('name', 'Name', 'required');
			if ($this->form_validation->run() == TRUE) {
				$category = $this->input->post();
				$category_registrated = $this->Category_Model->saveCategory($category['name']);
				if($category_registrated) {
					redirect(site_url(['Category','categories']));
				}
			}else{
			redirect(site_url(['Category','addCategoryV']));
			}
		}

    }
    //Getting data for edit category of the database
    public function editCategory(){
        $this->load->library('form_validation');
        $edit_category = $this->input->post('update');
        if(isset($edit_category)) {
			$this->form_validation->set_rules('idUpdate', 'Id', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            if ($this->form_validation->run() == TRUE) {
				$n_Category = $this->input->post();
				$category_edited = $this->Category_Model->editCategory($n_Category['idUpdate'],$n_Category['name']);
                if($category_edited) {
					redirect(site_url(['Category','categories']));
				}
			}else{
			redirect(site_url(['Category','editCategoryV']));
			}
        }
    }
    //Getting data of form for delete category in the database
    public function deleteCategory($id){
        $inNews = $this->News_Model->categoryINnews($id);
        if($inNews){
        $category_deleted = $this->Category_Model->deleteCategory($id);
        if($category_deleted) {
            redirect(site_url(['Category','categories']));
        }
        }else{
            $this->load->view('error');
        }
    }

}