<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    ///Function reads a xml
    public function addFeed($feedURL,$cat,$us){
        $sql="INSERT INTO `news`(`title`, `short_description`, `permanlink`, `imagen_url`, `sdate`, `user_id`, `category_id`)
        VALUES";
        $url = str_replace('$','/',$feedURL); 
        $rss = simplexml_load_file($url); 
            foreach($rss->channel->item as $item) { 
                $link = $item->link;  //extrae el link
                $title = $item->title;  //extrae el titulo
                //causar algun problema a la hora de cargar la consulta
                $title = preg_replace('([^A-Za-z0-9Ññ :;úóíáéÁÉÍÓÚ?¿!¡+*$%#@.-_])', '', $title);//Quita los caracteres que pueden 
                date_default_timezone_set("America/Costa_Rica");//Define la zona horaria de costa rica en este caso
                $date =date('Y-m-d h:i:s', time());//formato de fecha y hora
                //Con este if se busca la imagen del rss
                if(is_array(($item->xpath('media:thumbnail')))){
                    $mediaArray = $item->xpath('media:thumbnail');                                
                    $media = end($mediaArray);                                              
                    $url_imagen = $media->attributes()->url;
                }
                //Si no hay imagen se define el atributo como NO para indicar que no posee y cargar una predeterminada
                if($url_imagen==NULL){
                    $url_imagen="NO";
                }
                $description = strip_tags($item->description);  //extrae la descripcion
                $description = preg_replace('([^A-Za-z0-9 :;úóíáé?¿!¡+*$%#@.-_])', '', $description);//quita los caracters que pueden interferir en query
                if (strlen($description) > 200) { //limita la descripcion a 200 caracteres
                $stringCut = substr($description, 0, 200);                   
                $description = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';}
                $sql.="('$title','$description', '$link','$url_imagen', '$date', '$us','$cat'),";
                }
                $sql = substr($sql, 0, -1);
                $news_registrated = $this->News_Model->feedBD($sql);
                if($news_registrated) {
					redirect(site_url(['NewSource','sources']));
				}
        }
}