<?php
include_once('functions.php');
///Save the news obtained through rss in the database
function feedBD($sql){
    $connection = getConnection();
     if (mysqli_multi_query($connection, $sql)) {
        echo $sql;
    } else {
        echo "Error: " . $sql . "<br>" . $connection->error;
    }
    $connection->close();
}