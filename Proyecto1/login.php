<?php
include('functions.php');//include the class functions where the functions are located 
//
if(isset($_POST['login'])){
    $email = $_REQUEST['email'];
    $password = $_REQUEST['pass'];
    $user = authenticate($email, $password);//Call the method for validate the infomación
    if($user){
        session_start();
        $_SESSION['user'] = $user;
        if($user['name'] === 'Administrador'){
            header('Location: categories.php');
        } else{
            header('Location: paginaInicio.php');
        }
        
    } else {
        header('Location: index.php');
    }
}
?>