<?php
//Getting data of form for add category in the database
include('functions.php');
if(isset($_POST['save'])){
    $n_Category = $_REQUEST['name'];
    addCategory($n_Category);
}
//Getting data of form for delete category in the database
if(isset($_REQUEST['idD'])){
    $id = $_REQUEST['idD'];
    deleteCategory($id);
}
//Getting data of form for search category
$idUpdate ="";
if(isset($_REQUEST['idE'])){
    $id = $_REQUEST['idE'];
    $idUpdate = $_REQUEST['idE'];
    searchCategory($id);
}
//Getting data for edit category of the database
if(isset($_POST['update'])){
    $idUpdate = $_REQUEST['idUpdate'];
    $nameUpdate =$_REQUEST['name'];
    editCategory($idUpdate, $nameUpdate);
}



//Add category to the database
function addCategory($n_Category){
    $connection = getConnection();
    $sql = "INSERT INTO `category`(`id_category`, `name_category`) VALUES ('','$n_Category')";
    $query = mysqli_query($connection, $sql);
    if(!$query){
        die("Query Failed");
    }
    header("Location: categories.php"); //redireccionar
}
//Delete category of the database
function deleteCategory($id){
    $connection = getConnection();
    $sql = "DELETE FROM category WHERE id_Category = $id";
    $query = mysqli_query($connection, $sql);
    if($query){
        $_SESSION['message'] = 'Deleted category!';
        header("Location: categories.php"); //redireccionar
    }else{
        header("Location: error.html");
    }
}

//Search category to edit

function searchCategory($id){
    $connection = getConnection();
    $query = "SELECT * FROM category WHERE id_category = $id";
    $sql = mysqli_query($connection, $query);

    if(mysqli_num_rows($sql) == 1){ //si el select tiene como resultado 1 con ese id
        $row = mysqli_fetch_array($sql);
        header("Location: editCategory.php?idU=".$row['id_category']."&nameU=".$row['name_category']."");
    }
}

//Edit database category
function editCategory($idUpdate, $nameUpdate){
    $connection = getConnection();
    $sql = "UPDATE category SET `name_category` = '$nameUpdate' WHERE id_category = '$idUpdate'";
    $query= mysqli_query($connection, $sql);
    if($query){
        $_SESSION['message'] = 'Updated category!';
        header("Location: categories.php"); //redireccionar
    }


}
?>

