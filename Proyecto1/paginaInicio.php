<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="css/estilos.css">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">
</head>
<?php
include('functions.php');
$connection = getConnection();
session_start();
$user = $_SESSION['user'];
    if (!$user) {
         header('Location: index.php');
    }
    if($user['name'] === 'Administrador'){
        header('Location: categories.php');
    }
?>
<body>
<div class = "container">
    <div class = "row">
        <div class= "col-md-11">
            <div class = "moverImgMycLogin">
                <img src="img/ncover.png" alt="">
            </div>
        </div>
        <div class = "col-md-1">
            <input type="button" class="btn-md btnAdmin" value="<?php echo $user['first_name'];?>"> 
            <a href="logout.php"><input type="button" class="btn-md btnLogout" value="Logout"></a>
            <a href="nsources.php"><input type="button" class="btn-md btnCategories" value="New Sources"></a>
        </div>
    </div>
    <header class = "row">
        <div class = "col-md-12 text-center">
            <h2>Your Unique News Cover</h1>
        </div>
    </header>
    <div class = "row justify-content-center">
        <div class="col-md-3">
            <hr>
        </div>
    </div>
<?php
    $categoryID = 0;
    if(isset($_POST['categoriaID'])){
        $array = explode('*',$_POST['categoriaID']);
        $categoryID=$array[0];
        $usuarioID=$array[1];
    }
?>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class= "btn btn-outline-dark btn-md 
                <?php if($categoryID!=0){
                    echo "0";
                }else{
                    echo "active";
                }?> ">
                    <input type="radio" name="categoriaID" onchange="this.form.submit()" value ="0*<?php echo $user['id_User']?>">Portal
                </label>
        <?php
            //Query that filters the new sources according to the logged in user
            $sql = "SELECT DISTINCT news.category_id, category.name_category FROM news 
                    INNER JOIN category ON news.category_id = category.id_category WHERE news.user_id = ".$user['id_User']."";
            $query = mysqli_query($connection, $sql);
            //recorre las filas
            while($row = mysqli_fetch_array($query)){
                ///Generate the category separation buttons
                    echo '<label class= "btn btn-outline-dark btn-md">';
                    echo '<input type="radio" name="categoriaID" autocomplete = "off" onchange="this.form.submit()" value = "'
                    .$row['category_id']."*".$user['id_User'].'">'.$row['name_category'].'';
                    echo "</label>";
                 }?>
    </div>
    </form>
    <div class = "row">
        <?php
            //Query that filters the new sources according to the logged in user
            if($categoryID !=0){
                $sql= "SELECT news.id_news, news.title, news.short_description, news.permanlink, 
                news.imagen_url,news.sdate, category.name_category
                FROM news INNER JOIN category ON news.category_id = category.id_category  
                WHERE news.user_id = $usuarioID AND news.category_id = $categoryID
                ORDER BY news.sdate DESC";
                $query = mysqli_query($connection, $sql);
                $numero= mysqli_num_rows ($query);//Cuenta la cantidad de filas traida por la consulta
                if($numero===0){//Si el numero de filas que trae es cero significa que no tiene noticias entonces
                    // lo redirecciona a agregar una nueva fuente para que tenga noticias
                    header("Location:addNewSources.php");
                }
                //recorre las filas
                while($row = mysqli_fetch_array($query)){
                    $img=$row['imagen_url'];
                    if($img=="NO"){
                        $img= "img/reg.png";      
                    }
                    echo '<div class = "col-md-4">
                            <div class = "row">
                                <div class = col-md-5>
                                    <p class = "nFecha">'.$row['sdate'].'</p>
                                </div>
                            </div>
                            <div class = "row cont">
                                <div class = "col-md-6 ex2">
                                    <a href="'.$row['permanlink'].'"><img class = "redimension" src="'.$img.'" alt=""></a>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = col-md-8>
                                    <a href="'.$row['permanlink'].'"><p class = "nTitulo"><strong>'.$row['title'].'</strong></p></a>
                                </div>
                                <div class = col-md-4>
                                <p class = "nCategoria">'.$row['name_category'].'</p>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = col-md-10>
                                    <p class =  "nDescripcion">'.$row['short_description'].'</p>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = col-md-6>
                                    <a href="'.$row['permanlink'].'">Ver Noticia</a>
                                </div>
                            </div>
                        </div>';  
            }}else{
                $user2= $user['id_User'];
                $sql= "SELECT news.id_news, news.title, news.short_description, news.permanlink, 
                news.imagen_url,news.sdate, category.name_category FROM news, category 
                WHERE news.user_id = $user2 AND news.category_id = category.id_category 
                ORDER BY news.sdate DESC";
                $query = mysqli_query($connection, $sql);
                $numero= mysqli_num_rows ($query);//Cuenta la cantidad de filas traida por la consulta
                if($numero===0){//Si el numero de filas que trae es cero significa que no tiene noticias entonces
                    // lo redirecciona a agregar una nueva fuente para que tenga noticias
                    header("Location:addNewSources.php");
                }
                //recorre las filas
                while($row = mysqli_fetch_array($query)){
                    $img=$row['imagen_url'];
                    if($img=="NO"){
                        $img= "img/reg.png";      
                    }
                    echo '<div class = "col-md-4">
                            <div class = "row">
                                <div class = col-md-5>
                                    <p class = "nFecha">'.$row['sdate'].'</p>
                                </div>
                            </div>
                        <div class = "row cont">
                            <div class = "col-md-6 ex2">
                                <a href="'.$row['permanlink'].'"><img class = "redimension" src="'.$img.'" alt=""></a>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = col-md-8>
                            <a href="'.$row['permanlink'].'"><p class = "nTitulo"><strong>'.$row['title'].'</strong></p></a>
                        </div>
                            <div class = col-md-4>
                                <p class = "nCategoria">'.$row['name_category'].'</p>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = col-md-10>
                                <p class =  "nDescripcion">'.$row['short_description'].'</p>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = col-md-6>
                                <a href="'.$row['permanlink'].'">Ver Noticia</a>
                            </div>
                        </div>
                    </div>'; 
                }

            }?>
    
    </div>
</div>
<hr>
<nav>
    <div id = "barraBajaLogin"> 
        <ul>
            <li><a href="inicio.html" >My cover</a></li> 
            <li><a href="#" >|</a></li>
            <li><a href="cambalache.html" >About</a></li>
            <li><a href="#" >|</a></li>
            <li><a href="login.html" >Help</a></li>
        </ul>
    </div>
</nav>
<footer>
    <div id = "imgLoginC">
        <img src="img/c.png" alt="My news cover">
        <h3>My news cover</h3>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
</body>
</html>