<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add New Sources</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="css/estilos.css">
</head>
<?php
include('functions.php');
$conexion = getConnection();
session_start();
$user = $_SESSION['user'];
    if (!$user) {
         header('Location: index.php');
    }
    if($user['name'] === 'Administrador'){
        header('Location: categories.php');
    }
?>
<body>
<div class = "container">
    <div class = "row">
        <div class= "col-md-11">
            <div class = "moverImgMycLogin">
                <img src="img/ncover.png" alt="">
            </div>
        </div>
        <div class = "col-md-1">
            <input type="button" class="btn-md btnAdmin" value="<?php echo $user['first_name'];?>"> 
            <a href="logout.php"><input type="button" class="btn-md btnLogout" value="Logout"></a>
            <a href="nsources.php"><input type="button" class="btn-md btnCategories" value="New Sources"></a>
        </div>
    </div>
    <div class = "row">
        <div class = "col-md-3">
            <h1>Add New Source</h1>
        </div>
    </div>
    <div class = "row">
            <div class = "col-md-6">
                <div class = "hrLogin1">
                    <hr>
                </div>
            </div>
        </div>
        <div class = "row">
            <div class = "col-md-6">
            <form action="newsource.php" method="POST">
                <div class="form-group row">
                    <div class = col-md-6>
                    <input id="name" class="form-control inputLogin" type="text" name="name" placeholder="Name" autofocus>
                    </div>
                </div>
                <div class = "form-group row">
                    <div class = "col-md-12">
                        <input id="url" class="form-control" type="text" name="url" placeholder="URL RSS" autofocus>
                    </div>
                </div>
                <div class="form-group row">
                        <div class = "col-md-6">
                                <select id="cat" class="form-control" name="category">
                                    <?php
                                    // Realizamos la consulta para extraer los datos
                                    $consulta = "SELECT * FROM `category`";
                                    $resultado = mysqli_query( $conexion, $consulta );
                                        while ($columna = mysqli_fetch_array($resultado)) {
                                        // En esta sección estamos llenando el select con datos extraidos de una base de datos.
                                        echo '<option value="'.$columna['id_category'].'">'.$columna['name_category'].'</option>';
                                        }
                                    ?>
                                </select>
                        </div>
                    </div>
                    <div class = "hrLogin2">
                        <hr>
                    </div>
                    <input type="hidden" name="idU" value="<?php echo $user['id_User']?>" >
                <input type="submit" class="btn btn-secondary btn-sm btnLogin" name="addNewS" value="Save"> 
            </form> 
            </div>  
        </div>
    </div>
    <hr>
    <nav>
        <div id = "barraBajaLogin"> 
            <ul>
            <li><a href="inicio.html" >My cover</a></li> 
            <li><a href="#" >|</a></li>
            <li><a href="cambalache.html" >About</a></li>
            <li><a href="#" >|</a></li>
            <li><a href="login.html" >Help</a></li>
            </ul>
        </div>
    </nav>
    <footer>
        <div id = "imgLoginC">
            <img src="img/c.png" alt="My news cover">
                <h3>My news cover</h3>
        </div>
    </footer>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
</body>
</html>