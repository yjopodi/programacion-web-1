<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categories</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="css/estilos.css">
</head>
<?php
include('functions.php');
$connection = getConnection();
session_start();
$user = $_SESSION['user'];
    if (!$user) {
         header('Location: index.php');
    }
    if($user['name'] === 'Cliente'){
        header('Location: paginaInicio.php');
    }
?>
<body>
<div class = "container">
    <div class = "row">
        <div class= "col-md-11">
            <div class = "moverImgMycLogin">
                <img src="img/ncover.png" alt="">
            </div>
        </div>
        <div class = "col-md-1">
            <input type="button" class="btn-md btnAdmin" value="<?php echo $user['name'];?>"> 
            <a href="logout.php"><input type="button" class="btn-md btnLogout" value="Logout"></a>
            <a href="categories.php"><input type="button" class="btn-md btnCategories" value="Categories"></a>
        </div>
    </div>
    <div class = "row">
        <div class = "col-md-3">
            <h1>Categories</h1>
        </div>
    </div>
    <div class = "row">
            <div class = "col-md-6">
                <div class = "hrLogin1">
                    <hr>
                </div>
            </div>
    </div>
        <div class = "row">
            <div class = "col-md-4">
                <table class="table table-bordered">
                    <thead>
                        <tr>  
                            <th>Name</th>
                            <th><strong> Actions </strong></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sql = "SELECT * FROM `category`";
                        $query = mysqli_query($connection, $sql);
                        //recorre las filas
                            while($row = mysqli_fetch_array($query)){?>
                        <tr>
                            <td><?php echo $row['name_category']?></td>
                            <td>
                                <a href="category.php?idE=<?php echo $row['id_category'] //obtener id para editar?>" class="btn btn-link mova">
                                    <i class="fas fa-marker"></i>
                                </a> 
                                <a href="category.php?idD=<?php echo $row['id_category'] //obtener id para eliminar?>" class="btn btn-link mova">
                                    <i class="far fa-trash-alt"></i>
                                </a> 
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
                <a href="addCategory.php"><input type="button" class="btn btn-secondary btn-sm btnLogin" name="add" value="Add new"></a>
            </div>  
        </div>
    <hr>
</div>
<nav>
    <div id = "barraBajaLogin"> 
        <ul>
            <li><a href="inicio.html" >My cover</a></li> 
            <li><a href="#" >|</a></li>
            <li><a href="cambalache.html" >About</a></li>
            <li><a href="#" >|</a></li>
            <li><a href="login.html" >Help</a></li>
        </ul>
    </div>
</nav>
<footer>
    <div id = "imgLoginC">
        <img src="img/c.png" alt="My news cover">
        <h3>My news cover</h3>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
</body>
</html>