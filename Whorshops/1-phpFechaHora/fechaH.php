<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="fechaHora.css">
    <title>Fecha y hora</title>
</head>
<body>
<?php
date_default_timezone_set("America/Costa_Rica");
$fecha = date('d-m-Y');  
$hora = date('h:i:s a', time());
echo "<h1>$fecha</h1><h2>$hora</h2>"

?>
</body>
</html>