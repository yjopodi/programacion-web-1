<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
  header('Location: index.php');
}
?>
<?php include("includes/header.php");?>
<?php include("funciones.php");?>

<h1>Welcome <?php echo $user['first-name'];?> <?php echo $user['last-name'] ?></h1>
<style>
h1 {
    text-align: center; 
    margin-top: 60px;
}
</style>
<?php include("includes/footer.php");?>