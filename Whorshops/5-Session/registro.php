<?php 
include("includes/header.php");
include('funciones.php');
?>
<div class="container p-4"> 
    <div class="row">
        <div class="col-md-4 mx-auto">
            <?php session_start();
            if(isset($_SESSION['message'])){ ?>
            <script>
            Swal.fire('<?= $_SESSION['message']?>', '', 'success');
            </script>
            <?php session_unset();}?>
            <?php $user = $_SESSION['user'];?>
            <div class = "row centrar">
                <p>Bienvenido <?php echo $user['nombre']?> ingresa los datos de la matricula</p>
            </div>
            <div class="card-body">
                <form action="saveM.php" method="POST">
                    <div class="form-group">
                        <input id="identification" class="form-control" type="text" name="identification" placeholder="Identification" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="nombre" class="form-control" type="text" name="nombre" placeholder="First Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="apellidos" class="form-control" type="text" name="apellidos" placeholder="Last Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="email" class="form-control" type="text" name="email" placeholder="Email" autofocus>
                    </div>
                    <div class="form-group">
                        <select class="custom-select" id="inputGroupSelect01" name="carrera">
                            <?php
                            $careers = getCareers();
                            foreach($careers as $career) { ?>
                            <option value="<?php echo $career['carrera_id']?>"><?php echo $career['nombre']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save" value="Register">
                    <a class="btn btn-secondary" href="index.php" role="button">Salir</a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include("includes/footer.php");?>