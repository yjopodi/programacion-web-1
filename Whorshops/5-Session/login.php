<?php
include('funciones.php');

if(isset($_POST['login'])){
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];

    $user = authenticate($username, $password);

    if($user){
        session_start();
        $_SESSION['user'] = $user;
        if($user['role'] === 'admin'){
            header('Location: registro.php');
        } else{
            header('Location: inicio.php');
        }
        
    } else {
        header('Location: index.php');
    }
}
?>