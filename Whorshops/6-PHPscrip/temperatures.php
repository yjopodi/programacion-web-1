<?php
include("functions.php");

$temperatures = temperatures();
$arrayNoDuplicates = [];
foreach($temperatures as $index => $temperature) {
    if (!in_array($temperature, $arrayNoDuplicates)) {
        $arrayNoDuplicates[$index] = $temperature;
    }
}

$average = 0;
$sum = 0;
foreach ($arrayNoDuplicates as $value) {
    //echo($value.PHP_EOL);
    $sum += $value;
}
$average = ($sum/count($arrayNoDuplicates));
echo("\n\n\nThe average is: $average");

$arrayNoDuplicatesT = $arrayNoDuplicates;
echo("\n\n\nLower temperatures: ");
for ($i=0; $i <5 ; $i++) { 
    $numTemp = min($arrayNoDuplicates);
    $index = array_search($numTemp, $arrayNoDuplicates);
    unset($arrayNoDuplicates[$index]);
    echo("$numTemp ");
}
echo("\n\n\nHigher temperatures: ");
for ($i=0; $i <5 ; $i++) { 
    $numTemp = max($arrayNoDuplicatesT);
    $index = array_search($numTemp, $arrayNoDuplicatesT);
    unset($arrayNoDuplicatesT[$index]);
    echo("$numTemp ");
    if($i >3){
        echo("\n\n\n");
    }
}
?>