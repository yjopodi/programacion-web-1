<?php
include("db.php");
include("includes/header.php");
?>
<div class="container p-4">
    <div class="row">
    <div class="col-md-10">
            <table class="table table-bordered">
                <thead>
                    <tr>  
                        <th>Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Career</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $query = "SELECT * FROM matricula";
                        $sql = mysqli_query($conexion, $query);
                        //recorre las filas
                        while($row = mysqli_fetch_array($sql)){
                            $id = $row['id_carrera'];
                            $consulta = "SELECT * FROM carreras WHERE carrera_id = '$id'";
                            $resultado = mysqli_query($conexion, $consulta);
                            //recorre las filas
                            while($columna = mysqli_fetch_array($resultado)){?>
                            
                            <tr>
                                <td><?php echo $row['nombre']?></td>
                                <td><?php echo $row['apellidos']?></td>
                                <td><?php echo $row['correo']?></td>
                                <td><?php echo $columna['nombre']?></td>
                                <td><?php echo $row['fecha']?></td>
                            </tr>
                        <?php }}?>
                </tbody>
            </table>
        </div>
    
    </div>
    <div>
    <a class="btn btn-dark" href="index.php" role="button">Regresar</a>
    </div>

</div>


<?php include("includes/footer.php");?>