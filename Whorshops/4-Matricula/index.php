<?php
include("db.php");
include("includes/header.php");
?>
<div class="container p-4">
    <div class="row">
        <div class="col-md-6">

        <?php if(isset($_SESSION['message'])){ ?>
        <script>Swal.fire('<?= $_SESSION['message']?>', '', 'success');</script>
        <?php session_unset();}?> 

            <div class="card card-body">
                <form action="save.php" method="POST">
                    <div class="form-group">
                        <input id="identification" class="form-control" type="text" name="identification" placeholder="Identification" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="nombre" class="form-control" type="text" name="nombre" placeholder="First Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="apellidos" class="form-control" type="text" name="apellidos" placeholder="Last Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="email" class="form-control" type="text" name="email" placeholder="Email" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="carrera">Carrera</label>
                        <select id="carrera" class="form-control" name="carrera">
                        <?php
                            // Realizamos la consulta para extraer los datos
                            $consulta = "SELECT * FROM carreras";
                            $resultado = mysqli_query( $conexion, $consulta );
                            while ($columna = mysqli_fetch_array($resultado)) {
                            // En esta sección estamos llenando el select con datos extraidos de una base de datos.
                                echo '<option value="'.$columna['carrera_id'].'">'.$columna['codigo'].'-'.$columna['nombre'].'</option>';
                            }
                        ?>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save" value="Save"> 
                </form>
            </div>
        </div>
        <div class="col-md-6">
        <img src="images/estd.jpg" alt="">
        </div>
    </div>
    <div class="boton">
    <a class="btn btn-info" href="viewMatriculas.php" role="button">Matriculados</a>
    </div>
    
</div>