<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function home()
	{
		$this->load->library('form_validation');
		$register_user = $this->input->post('save');
		if(isset($register_user)) {
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
	
			if ($this->form_validation->run() == TRUE) {
				$user = $this->input->post();
				$user_registrated = $this->User_model->saveUser($user);

				if($user_registrated) {
					redirect(site_url(['user','home']));
				}
			}
			redirect(site_url(['user','home']));
		}
		$users = $this->User_model->getAll();
		$data['users'] = $users;
		$this->load->view('home', $data);
    }

	public function delete($id){
		$delete = $this->User_model->deleteById($id);
		redirect(site_url(['user','home']));
	}

	public function edit($id){
	}
}