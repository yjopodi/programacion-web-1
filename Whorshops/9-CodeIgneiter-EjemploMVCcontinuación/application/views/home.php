<?php include("application/views/includes/header.php");
?>

<div class="container p-4">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-body" id="title_category">
                <h2>Register User</h2>
            </div>
            <div class="card card-body">
                <form action="<?php echo site_url(['user','home']);?>" method="POST">
                    <div class="form-group">
                        <input id="name" class="form-control" type="text" name="first_name" placeholder="First Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="name" class="form-control" type="text" name="last_name" placeholder="Last Name" autofocus>
                    </div>
                    <div class="form-group">
                        <input id="description" class="form-control" type="text" name="email"
                            placeholder="Email" autofocus>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save" id="save_category" value="Save">
                </form>
            </div>
        </div><br>
        <div class="col-md-8">
            <table class="table table-bordered">
                <thead>
                    <tr class="">
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($users as $user) {?>
                    <tr class="table-light">
                        <td><?php echo $user['first-name']?></td>
                        <td><?php echo $user['last-name']?></td>
                        <td><?php echo $user['email']?></td>
                        <td>
                            <a href="<?php echo site_url(['user','edit',$user['id']]);?>" class="btn btn-primary">
                                <i class="fas fa-marker"></i>
                            </a>
                            <a href="<?php echo site_url(['user','delete',$user['id']]);?>" class="btn btn-danger">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include("application/views/includes/footer.php");?>