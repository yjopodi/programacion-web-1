<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function saveUser($user){
        $this->db->set('first_Name', $user['first_name']);
        $this->db->set('last_Name', $user['last_name']);
        $this->db->set('email', $user['email']);
        $query = $this->db->insert('users');
        if ($this->db->affected_rows() > 0) {
          return true;
        }else {
          return false;
        }
      }
      
      public function getAll(){
        $query = $this->db->get('users');
        return $query->result_array();
    }

    public function deleteById($id){
        $this->db->where('id', $id);
        $query = $this->db->delete('users');
        if($query){
            return true;
        } else {
        return false;
        }
    }

    

  }